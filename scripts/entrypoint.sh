#!/bin/sh
# exit immediately if error occurs
set -e

python manage.py collectstatic --noinput
# This allows you to collect all of the static files required for your project and store them in a single
# directory. and noinput means it doesn't ask any input from us enter yes for everything
python manage.py wait_for_db
#if any changes occurs it wait for db response
python manage.py migrate
#it runs any migration on our project
uwsgi --socket :9000 --workers 4 -master --enable-threads --module app.wsgi
# uwsgau = name of the application
# run uwsgi as tcp socket on port 9000
#run this as a master service on the terminal
#it uses multi threads and runs the module app.uswgi