data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["al2023-ami-2023.2.*-x86_64"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "basition" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-basition")
  )


}
