terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-sugumar-tfstate"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-sugumar-tfstate-lock"
    #primary_key      =   LockID
  }
}

# terraform {
#   required_providers {
#     aws = {
#       region  = "us-east-1"
#       source = "hashicorp/aws"
#       version = "~> 2.54.0"  # Replace with your desired version constraint
#     }
#   }
# }
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
  #version = ">= 2.0, < 3.0"

}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"

  }

}